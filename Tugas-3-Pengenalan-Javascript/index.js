//Soal 1
// buatlah variabel-variabel seperti di bawah ini

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// gabungkan variabel-variabel tersebut agar menghasilkan output

// saya senang belajar JAVASCRIPT

var pertama1 = pertama.substr(0,4)
console.log(pertama1)

var pertama2 = pertama.substr(12,6)
console.log(pertama2)

var gabung = pertama1.concat(" ",pertama2)
console.log(gabung)

var kedua1 = kedua.substr(0, 18)
console.log(kedua1)

var fix = gabung.concat(" ",kedua1)
console.log(fix)



//Soal 2


// buatlah variabel-variabel seperti di bawah ini

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
// *catatan :
// 1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
// 2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)


//...Jawaban Soal 2

var numberPertama = Number(kataPertama);
var numberKedua = Number(kataKedua);
var numberKetiga = Number(kataKetiga);
var numberKeempat = Number(kataKeempat);

console.log(typeof numberPertama);
console.log(typeof numberKedua);
console.log(typeof numberKetiga);
console.log(typeof numberKeempat);

var operasipertama = numberPertama + numberKedua * numberKetiga + numberKeempat
console.log(operasipertama)
var operasikedua = (numberKedua + numberKetiga) * (numberPertama % numberKeempat)
console.log(operasikedua)
var operasiketiga = (numberPertama - numberKetiga + numberKeempat) * numberKedua
console.log(operasiketiga)
 var operasikeempat = numberKeempat * numberKedua * (numberPertama % numberKetiga)
 console.log(operasikeempat)

//Soal 3

// buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 




// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

//...Jawaban Soal 3

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 


// var kataKedua; = kalimat.substring(4,10); 
// var kataKetiga; = kalimat.substring(15,3); 
// var kataKeempat; = kalimat.substring(19,5); 
// var kataKelima; = kalimat.substring(25,6); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);