// soal 1

// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

//.... jawaban soal 1

var nilai = 65;

if(nilai >= 85){
    console.log("indeksnya A")
} else if(nilai >= 75 && nilai < 85){
    console.log("indeksnya B")
} else if(nilai >= 65 && nilai < 75){
    console.log("indeksnya C")
} else if(nilai >= 55 && nilai < 65){
    console.log("indeksnya D")
} else if(nilai >= 55){
    console.log("indeksnya E")
} else {
    console.log("Nilai tidak terdapat dalam index manapun")
}

// soal 2

// buatlah variabel seperti di bawah ini

var tanggal = 01;
var bulan = 11;
var tahun = 1995;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

//.... jawaban soal 2

switch(bulan){
    case 1: {console.log("01 november 1995"); break;}
    case 2: {console.log("02 november 1995"); break;}
    case 3: {console.log("03 november 1995"); break;}
    case 4: {console.log("04 november 1995"); break;}
    case 5: {console.log("05 november 1995"); break;}
    case 6: {console.log("06 november 1995"); break;}
    case 7: {console.log("07 november 1995"); break;}
    case 8: {console.log("08 november 1995"); break;}
    case 9: {console.log("09 november 1995"); break;}
    case 10: {console.log("10 november 1995"); break;}
    case 11: {console.log("11 november 1995"); break;}
    default: {console.log("Tidak Ada Tanggal lahir"); break;}
    
}



// soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output untuk n=3 :

// #
// ##
// ###
// Output untuk n=7 :

// #
// ##
// ###
// ####
// #####
// ######
// #######

//.... jawaban soal 3\

var n = 3
var e = ''
for (var i = 0; i <=  n; i++) {
    for(var j = 0; j < i; j++){
        e += '#'
    }
    
        e += '\n'

}

console.log(e)




// soal 4

// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// contoh :

// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

// Output untuk m = 5

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript

// Output untuk m = 7

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming


// Output untuk m = 10

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming
// 8 - I love Javascript
// 9 - I love VueJS
// =========
// 10 - I love programming


//.... jawaban soal 4

var m = 5
for(var i = 1; i <=  m; i++){
    if(i == 3){
        console.log('1 - I Love programming');
        console.log('2 - I Javascript');
        console.log('2 - I Love vueJS');
        console.log('===');
    }else if(i == 5){
        console.log('1 - I Love programming');
        console.log('2 - I Javascript');
        console.log('2 - I Love vueJS');
        console.log('===');
        console.log('4 - I Love programming');
        console.log('5 - I Javascript');
    }else if( i == 7){
        console.log('1 - I Love programming');
        console.log('2 - I Javascript');
        console.log('2 - I Love vueJS');
        console.log('===');
        console.log('4 - I Love programming');
        console.log('5 - I Javascript');
        console.log('6 - I Love vueJS');
        console.log('======');  
        console.log('4 - I Love programming');
        console.log('5 - I Javascript');
        console.log('6 - I Love vueJS');
    }else if( i == 10){
        console.log('1 - I Love programming');
        console.log('2 - I Javascript');
        console.log('2 - I Love vueJS');
        console.log('===');
        console.log('4 - I Love programming');
        console.log('5 - I Javascript');
        console.log('6 - I Love vueJS');
        console.log('======');  
        console.log('7 - I Love programming');
        console.log('8 - I Javascript');
        console.log('9 - I Love vueJS');
        console.log('=========');  
    }
    
    
}